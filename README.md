# AWS GitLab CICD

This project contains templates for the creation of IAM Roles and an OIDC IDP within AWS Account(s) to support the authentication of GitLab-CI operations on AWS Account(s).

## Artifacts

The Templates in this project are:

- OIDC IDP Only ([idp-gitlab-oidc.yml](idp-gitlab-oidc.yml)) - Can be used by any future IAM roles where desired.
- Read-Only Role ([role-gitlab-read.yml](role-gitlab-read.yml)) - Used to read-only information from any account where deployed.
- Delivery Role ([role-gitlab-deliver.yml](role-gitlab-deliver.yml)) - Used to provide permissions to S3 buckets for delivery of deployment templates.
- Deploy Role ([role-gitlab-deploy.yml](role-gitlab-deploy.yml)) - Used run deploy operation within an account.
- Full Deployment ([role-gitlab-cicd.yml](role-gitlab-cicd.yml)) - Deploys the OIDC IDP and all Roles in a single deployment.

## Deployment

The deployment of these templates are required as a pre requirement to any future automation. Once these resources or deployed CICD can be run from individual GitLab Repositories.

These template(s) can be deployed individual to an account or from the management account to be delivered to every account in the organization.

> Note: The GitLab Group, Repo Names, and Role Names are Case Sensitive for STS to authenticate.
